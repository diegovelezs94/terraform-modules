module "VPC" {
  source          = "../../modules/Azure/VPC/"
  azure_rg_region = "${var.azure_rg_region}"
}
