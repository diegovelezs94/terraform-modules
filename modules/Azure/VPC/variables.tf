variable "azure_rg_name" {
  description = "Name for the resource group"
  default     = "network"
}

variable "azure_rg_region" {
  description = "Region to deploy the resources"
  default     = "West US"
}

variable "net_address_cidr" {
  description = "CIDR of the network"
  default     = "10.0.0.0/16"
}

variable "subnet_cidr" {
  type    = "list"
  default = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
}
