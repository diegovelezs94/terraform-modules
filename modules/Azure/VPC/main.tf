# Create a resource group
resource "azurerm_resource_group" "terraform_rg" {
  name     = "${var.azure_rg_name}"
  location = "${var.azure_rg_region}"
}

# Create a virtual network within the resource group
resource "azurerm_virtual_network" "vnet" {
  name                = "VPC-terraform"
  address_space       = ["${var.net_address_cidr}"]
  location            = "${azurerm_resource_group.terraform_rg.location}"
  resource_group_name = "${azurerm_resource_group.terraform_rg.name}"
}

# Loop based on the subnet index
resource "azurerm_subnet" "subvnet" {
  count                = "${length(var.subnet_cidr)}"
  name                 = "subnet-${count.index + 1}"
  resource_group_name  = "${azurerm_resource_group.terraform_rg.name}"
  virtual_network_name = "${azurerm_virtual_network.vnet.name}"
  address_prefix       = "${element(var.subnet_cidr, count.index)}"
}
